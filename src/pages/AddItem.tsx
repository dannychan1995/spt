import {
  IonButtons,
  IonContent,
  IonHeader,
  IonMenuButton,
  IonPage,
  IonTitle,
  IonToolbar,
  IonGrid,
  IonRow,
  IonCol,
  IonImg,

  IonList,
  IonItem,
  IonLabel,
  IonInput,
  IonToggle,
  IonRadio,
  IonCheckbox,
  IonItemSliding,
  IonItemOption,
  IonItemOptions,
  
  IonModal,
  useIonActionSheet,
  IonFab,
  IonFabButton,
  IonIcon,
  IonRippleEffect,
  IonButton,
} from '@ionic/react';
import {
  add,
} from 'ionicons/icons';
import TreeMenu from 'react-simple-tree-menu';

import { usePhotoGallery } from '../components/usePhotoGallery';
import { useState, useEffect } from 'react';

import { useParams } from 'react-router';
import ExploreContainer from '../components/ExploreContainer';
import './Page.css';

import { Drivers, Storage } from '@ionic/storage';
const ITEMS = 'items';
const USER_ID = '8888';
const store = new Storage({
  name: 'MyAppDB',
  dbKey: 'MyAppDB',
  driverOrder: [Drivers.IndexedDB, Drivers.LocalStorage]
});

const Page: React.FC = () => {
  const name = "信息";
  const { photos, takePhoto } = usePhotoGallery();
  // const [photos, setPhotos] = useState<UserPhoto[]>([]);
  const [present, dismiss] = useIonActionSheet();
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [seletedItem, setSeletedItem] = useState<any | null>();


  const treeData = [
    {
      key: '0',
      label: '會面',
      nodes: [
        {
          key: '00',
          label: '協議',
          nodes: [
            {
              key: '000',
              label: '地點',
              nodes: [
                {
                  key: '0001',
                  data: '0001',
                  label: '會議室1',
                },
                {
                  key: '0002',
                  data: '0002',
                  label: '會議室2',
                },
                {
                  key: '0003',
                  data: '0003',
                  label: '會議室3',
                },
              ] // you can remove the nodes property or leave it as an empty array
            },
          ],
        },
        {
          key: 'second-level-node-1',
          label: '買賣',
          nodes: [
    
                {
                  key: '001',
                  label: '地點',
                  nodes: [
                    {
                      key: '0011',
                      data: '0011',
                      label: '會議室1',
                    },
                    {
                      key: '0012',
                      data: '0012',
                      label: '會議室2',
                    },
                    {
                      key: '0013',
                      data: '0013',
                      label: '會議室3',
                    },
                  ]

            },
          ],
        },
      ],
    },
    {
      key: '1',
      label: '請假',
      nodes: [
        {
          key: '10',
          label: '病假',
          nodes: [
            {
              key: '100',
              label: '有薪病假',
              nodes: [
                {
                  key: '1001',
                  data: '1001',
                  label: '感冒',
                },
                {
                  key: '1002',
                  data: '1002',
                  label: '肚痛',
                },
                {
                  key: '1003',
                  data: '1003',
                  label: '工傷',
                },
              ] // you can remove the nodes property or leave it as an empty array
            },
            {
              key: '101',
              label: '無薪病假',
              nodes: [
                {
                  key: '1011',
                  data: '1011',
                  label: '覆診',
                },
                {
                  key: '1012',
                  data: '1012',
                  label: '個人原因',
                },
              ] // you can remove the nodes property or leave it as an empty array
            },
          ],
        },
        {
          key: '11',
          label: '事假',
          nodes: [
                {
                  key: '111',
                  label: '無薪事假',
                  nodes: [
                    {
                      key: '1111',
                      data: '1111',
                      label: '個人原因',
                    },
                    {
                      key: '1112',
                      data: '1112',
                      label: '結婚',
                    }
                  ]

            },
          ],
        },
      ],
    },
  ];

  const [items, setItems] = useState<any[]>([]);
  const [currentFilepath, setcurrentFilepath] = useState<any>();

  
  useEffect(() => {

    const loadSaved = async () => {
      await store.create();

      const value= await store.get(ITEMS);
      
      const itemsInStorage = (value ? value : []) as [];

      setItems(itemsInStorage);

      
    };
    loadSaved();
  }, []);

  const clickMe = async () => {
    const { filepath } = await takePhoto();
    
    setcurrentFilepath(filepath);
    setOpenModal(true);
    
  }

  const clickAdd = async () => {

    const { data } = seletedItem;
    const countExistingItem = items.reduce((pre, it) => pre + (it.item.data == data ? 1 : 0), 0);
    const newSerialCode = `${data}${USER_ID}${String(countExistingItem + 1).padStart(4, '0')}`
    const nodeIds = seletedItem.key.split('/');
    const levelLabels = [];
    let parrentNode:any[] = treeData;
    for (let i = 0; i < nodeIds.length; i++) {
      const currentNodeId = nodeIds[i];
      const currentNode = parrentNode.find((n) => n.key === currentNodeId);
      parrentNode = currentNode.nodes;
      levelLabels.push(currentNode.label)
    }
    console.log(levelLabels);
    
    const savedFileImage = {
      filepath: currentFilepath,
      item: seletedItem,
      serialCode: newSerialCode,
      timestamp: new Date(),
      levelLabel: levelLabels.join(' -> ')
    };
    const newPhotos = [...items, savedFileImage];
    setItems(newPhotos);
    await store.set(ITEMS, newPhotos)
    setOpenModal(false);
    setSeletedItem(null);
    
  }

  const clickTree = (data: any) => {
    setSeletedItem(data);
  }

  // const canDismiss: any = () => {
  //   return new Promise(async (resolve) => {
  //     await present({
  //       header: 'Are you sure you want to discard your changes?',
  //       buttons: [
  //         {
  //           text: 'Discard Changes',
  //           role: 'destructive'
  //         },
  //         {
  //           text: 'Keep Editing',
  //           role: 'cancel'
  //         }
  //       ],
  //       onDidDismiss: (ev: CustomEvent) => {
  //           const role = ev.detail.role;
            
  //           if (role === 'destructive') {
  //             resolve(true);
  //           }
            
  //           resolve(false);
  //       }
  //     });
  //   });
  // };

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>{name}</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen>
      
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">{name}</IonTitle>
          </IonToolbar>
        </IonHeader>
        
        {/* <IonButton id="trigger-button">Click to open modal</IonButton> */}
        <IonModal isOpen={openModal}>
        {/* <IonModal isOpen={true}> */}
          <IonContent>
            {seletedItem && <IonFab vertical="top" horizontal="end" slot="fixed">
              <IonFabButton onClick={clickAdd}>
                <IonIcon icon={add} />
              </IonFabButton>
            </IonFab>}
          <TreeMenu data={treeData} onClickItem={clickTree}/>
          </IonContent>
        </IonModal>
        <IonList>

         {items.map((it, i) => ( <IonItemSliding key={i}>
           <IonItem routerLink={`/item/${it.serialCode}`}>
             <IonLabel>{it.serialCode}
                {/* <IonImg src={photos.find((photo) => photo.filepath === it.filepath)?.webviewPath} /> */}
            </IonLabel>
            </IonItem>
            {/* <IonItemOptions side="end">
              <IonItemOption onClick={() => {}} color="danger">刪除</IonItemOption>
            </IonItemOptions> */}
          </IonItemSliding>))}
        </IonList>
        {/* <IonGrid>
          <IonRow>
            {photos.map((photo, index) => (
              <IonCol size="6" key={index}>
                <IonImg src={photo.webviewPath} />
              </IonCol>
            ))}
          </IonRow>
        </IonGrid> */}

        <IonFab vertical="bottom" horizontal="end" slot="fixed">
          <IonFabButton onClick={clickMe} >
            <IonIcon icon={add} />
          </IonFabButton>
        </IonFab>
      </IonContent>
    </IonPage>
  );
};

export default Page;
