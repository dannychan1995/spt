import {
  IonButtons,
  IonContent,
  IonHeader,
  IonMenuButton,
  IonPage,
  IonTitle,
  IonToolbar,
  IonImg,
  IonBackButton,
  IonNote,
  IonLabel,
  IonItem,
  IonItemGroup,
  IonItemDivider,
  IonButton,
  useIonAlert,
} from '@ionic/react';
import moment from 'moment';
import { useState, useEffect } from 'react';
import { RouteComponentProps} from 'react-router-dom';
import { useParams } from 'react-router';
import { usePhotoGallery } from '../components/usePhotoGallery';
import ExploreContainer from '../components/ExploreContainer';
import { Drivers, Storage } from '@ionic/storage';
import { FormBuilder as FormBuilderIo, Formio, FormEdit } from "@tsed/react-formio";
import { v4 as uuidv4 } from 'uuid';
import { useAppSelector, useAppDispatch } from '../store/hooks';
import {  increment } from '../reducer/counterSlice'

const ITEMS = 'items';
const FORMS_PREFIX = 'FORMS_';
const store = new Storage({
  name: 'MyAppDB',
  dbKey: 'MyAppDB',
  driverOrder: [Drivers.IndexedDB, Drivers.LocalStorage]
});

const Page: React.FC<RouteComponentProps> = (props) => {
  const { itemId } = useParams<{ itemId: string }>();
  const dispatch = useAppDispatch()

  const [item, setItem] = useState<any>();
  const [schema, setSchema] = useState<any>([]);
  const { photos, takePhoto } = usePhotoGallery();

  const loadSaved = async () => {
    await store.create();

    const value= await store.get(ITEMS);
    const myItem = value.find((it: { serialCode: string; }) => it.serialCode === itemId)
    myItem.photo = photos.find((photo) => photo.filepath === myItem.filepath)
    setItem(myItem);
    console.log(myItem);
    
  };
  useEffect(() => {
    loadSaved();
  }, []);
  useEffect(() => {
    loadSaved();
  }, [photos]);

  const [present] = useIonAlert();


  const saveForm = () => {
    present({
      cssClass: 'my-css',
      header: '創建表格',
      message: '請輸入表格名稱',
      inputs: [
        {
          name: 'name',
          type: 'text',
          placeholder: '表格名稱'
      },
    ],
      buttons: [
        'Cancel',
        {
          text: 'Ok', handler: async (d) => {
            const { name } = d;
            console.log(schema);
            const formKey = `${FORMS_PREFIX}${itemId}`
            const oldSchema = await store.get(formKey) || {};
            const formId = uuidv4(); 
            oldSchema[formId] = { name, schema, data: {} };
            await store.set(formKey, oldSchema);
            dispatch(increment())
            props.history.goBack();

        } },
      ],
      onDidDismiss: (e) => console.log('did dismiss'),
    })

    // 

  }
        
  // const formIoData = {
  //   display: "form",
  //   components: [
  //     {
  //       label: "Checkbox",
  //       tableView: false,
  //       key: "checkbox",
  //       type: "checkbox",
  //       input: true
  //     },
  
  //     {
  //       label: "Text Field",
  //       tableView: true,
  //       validate: {
  //         // pattern: "/^([A-Z][a-z .'-]*)*$/",
  //         customMessage: "Test error",
  //         // "custom": "valid = (input !== 'Joe')",
  //         minLength: 3,
  //         maxLength: 10
  //       },
  //       errorLabel: "Please fill in only letters.",
  //       key: "textField",
  //       type: "textfield",
  //       input: true
  //     },
  //     {
  //       type: "button",
  //       label: "Submit",
  //       key: "submit",
  //       disableOnInvalid: true,
  //       input: true,
  //       tableView: false
  //     }
  //   ]
  // };

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
        <IonButtons slot="start">
          <IonBackButton />
        </IonButtons>
        <IonButtons slot="end">
            <IonButton size="small" onClick={saveForm}>保存表格</IonButton>
        </IonButtons>
          <IonTitle>{itemId}</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">{itemId}</IonTitle>
          </IonToolbar>
        </IonHeader>
        <>
        <FormBuilderIo
            display={'form'} components={schema} onChange={(s) => { console.log(s);
             setSchema(s)} }
        />
        </>
      </IonContent>
    </IonPage>
  );
};

export default Page;
