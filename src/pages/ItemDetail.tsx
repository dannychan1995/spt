import {
  IonButtons,
  IonContent,
  IonHeader,
  IonMenuButton,
  IonPage,
  IonTitle,
  IonToolbar,
  IonImg,
  IonBackButton,
  IonNote,
  IonLabel,
  IonItem,
  IonItemGroup,
  IonItemDivider,
  IonButton,
} from '@ionic/react';
import moment from 'moment';
import { useState, useEffect } from 'react';
import { useParams } from 'react-router';
import { usePhotoGallery } from '../components/usePhotoGallery';

import { useAppSelector, useAppDispatch } from '../store/hooks';

import { Drivers, Storage } from '@ionic/storage';
import { log } from 'console';
const FORMS_PREFIX = 'FORMS_';
const ITEMS = 'items';
const store = new Storage({
  name: 'MyAppDB',
  dbKey: 'MyAppDB',
  driverOrder: [Drivers.IndexedDB, Drivers.LocalStorage]
});
const Page: React.FC = () => {

  const count = useAppSelector((state) => state.counter.value)
  const dispatch = useAppDispatch()

  const { itemId } = useParams<{ itemId: string }>();

  const [item, setItem] = useState<any>();
  const [schemas, setSchemas] = useState<any>([]);
  const { photos, takePhoto } = usePhotoGallery();

  const loadSaved = async () => {
    await store.create();

    const value= await store.get(ITEMS);
    const myItem = value.find((it: { serialCode: string; }) => it.serialCode === itemId)
    myItem.photo = photos.find((photo) => photo.filepath === myItem.filepath)
    setItem(myItem);

    console.log(myItem);

    const formKey = `${FORMS_PREFIX}${itemId}`
    const sch = await store.get(formKey) || {};
    setSchemas(sch)
    console.log(sch);
    
    
  };
  useEffect(() => {
    loadSaved();
  }, []);
  useEffect(() => {
    loadSaved();
  }, [photos]);
  useEffect(() => {
    loadSaved();
  }, [count, itemId]);


  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
        <IonButtons slot="start">
          <IonBackButton />
        </IonButtons>
          <IonTitle>{itemId}</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">{itemId}</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonImg src={item?.photo?.webviewPath} />
        {/* <ExploreContainer name={itemId} /> */}
        <IonItemGroup>
          <IonItemDivider>
            <IonLabel>分類</IonLabel>
          </IonItemDivider>
          <IonItem>
            <IonLabel>{item?.levelLabel}</IonLabel>
          </IonItem>
        </IonItemGroup>
        <IonItemGroup>
          <IonItemDivider>
            <IonLabel>時間</IonLabel>
          </IonItemDivider>
          <IonItem>
            <IonLabel>{moment(item?.timestamp).format('DD-MM-YYYY HH:mm:ss')}</IonLabel>
          </IonItem>
        </IonItemGroup>
        <IonItemGroup>
          <IonItemDivider>
            <IonLabel>表格</IonLabel>
          </IonItemDivider>
          <IonItem>
            {Object.keys(schemas).map((formKey, i) => (
              <IonButton key={i} expand="full" routerLink={`/formDetail/${itemId}/${formKey}`}>{ schemas[formKey].name}</IonButton>
            ))}
          </IonItem>
        </IonItemGroup>
        <IonButton expand="full" routerLink={`/formMaker/${itemId}`}>增加表格</IonButton>
      </IonContent>
    </IonPage>
  );
};

export default Page;
