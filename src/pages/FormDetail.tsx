import {
  IonButtons,
  IonContent,
  IonHeader,
  IonMenuButton,
  IonPage,
  IonTitle,
  IonToolbar,
} from '@ionic/react';
import { useParams } from 'react-router';
import './Page.css';
import { Drivers, Storage } from '@ionic/storage';
import { useState, useEffect } from 'react';
import {Form} from '@tsed/react-formio';

import { useAppSelector, useAppDispatch } from '../store/hooks';

const ITEMS = 'items';
const FORMS_PREFIX = 'FORMS_';
const store = new Storage({
  name: 'MyAppDB',
  dbKey: 'MyAppDB',
  driverOrder: [Drivers.IndexedDB, Drivers.LocalStorage]
});



const Page: React.FC = () => {
  const { itemId, formId } = useParams<{ itemId: string, formId: string  }>();

  const [formDetail, setFormDetail] = useState<any>({ schema: {}
    , data: {}
});

  const loadSaved = async () => {
    await store.create();

    const formKey = `${FORMS_PREFIX}${itemId}`
    const sch = await store.get(formKey) || {};
    setFormDetail(sch[formId])
    console.log(sch[formId]);
    
  };
  useEffect(() => {
    loadSaved();
  }, [formId]);

  const form = {
    display: 'form', 
    components: formDetail.schema,
  }

  const submission = {
    data: formDetail.data
  };


  const saveData = async (submission: any) => {
    console.log(submission);
    
    const {data} = submission
    console.log(formDetail.schema);
    const formKey = `${FORMS_PREFIX}${itemId}`
    const oldSchema = await store.get(formKey) || {};
    oldSchema[formId] = { ...formDetail, data }
    await store.set(formKey, oldSchema);

  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle></IonTitle>
        </IonToolbar>
      </IonHeader>

      {formDetail.name && <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">{formDetail.name}</IonTitle>
          </IonToolbar>
        </IonHeader>
        <Form submission={submission} form={form} onSubmit={saveData} />
      </IonContent>}
    </IonPage>
  );
};

export default Page;
