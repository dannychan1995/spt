import {
  IonApp,
  IonRouterOutlet,
  IonSplitPane,
  setupIonicReact,
} from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import { Redirect, Route } from 'react-router-dom';
import Menu from './components/Menu';
import Page from './pages/Page';
import Item from './pages/Item';
import AddItem from './pages/AddItem';
import DeleteItem from './pages/DeleteItem';
import ItemDetail from './pages/ItemDetail';
import FormMaker from './pages/formMaker';
import FormDetail from './pages/FormDetail';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';

setupIonicReact();

const App: React.FC = () => {
  return (
    <IonApp>
      <IonReactRouter>
        <IonSplitPane contentId="main">
          <Menu />
          <IonRouterOutlet id="main">
            <Route path="/" exact={true}>
              <Redirect to="/addItem" />
            </Route>
            <Route path="/item" exact={true}>
              <Item />
            </Route>
            <Route path="/addItem" exact={true}>
              <AddItem />
            </Route>
            <Route path="/DeleteItem" exact={true}>
              <DeleteItem />
            </Route>
            <Route path="/page/:name" exact={true}>
              <Page />
            </Route>
            <Route path="/item/:itemId" exact={true}>
              <ItemDetail />
            </Route>
            <Route path="/formMaker/:itemId" exact={true} component={FormMaker} />
            <Route path="/formDetail/:itemId/:formId" exact={true}>
              <FormDetail />
            </Route>
          </IonRouterOutlet>
        </IonSplitPane>
      </IonReactRouter>
    </IonApp>
  );
};

export default App;
